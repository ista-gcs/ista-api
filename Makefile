###########################################################
# Developer's Guide
###########################################################
#
# All tasks should be explicitly marked as .PHONY at the
# top of the section.
#
# We distinguish two types of tasks: private and public.
#
# "Public" tasks should be created with the description
# using ## comment format:
#
#   public-task: task-dependency ## Task description
#
# Private tasks should start with "_". There should be no
# description E.g.:
#
#   _private-task: task-dependency
#

###########################################################
# Setup
###########################################################

# Include .env file
ifneq (,$(wildcard ./.env))
    include .env
    export
endif

###########################################################
# Utils
###########################################################

#--------------------------------------
# Colors
#--------------------------------------

# Run messages (red)
_EC    := \033[0;31m
# Run messages (green)
_RC  := \033[0;32m
# Build messages (yellow)
_BC	    := \033[0;33m
# Setup and install messages (blue)
_SC   := \033[0;34m
# No coloring
_NC     := \033[0m

#--------------------------------------
# Printing
#--------------------------------------
# Output prefix
_OPX := "[MAKE] - "

###########################################################
# Project directories & paths
###########################################################

root_dir := $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
api_grpc_path := pkg/api/proto
proto_path := registry/
api_grpc_dir := $(root_dir)/$(api_grpc_path)
web_tests_dir := $(root_dir)/tests/web
dist_dir := $(root_dir)/dist
server_src_path := $(root_dir)/cmd/main.go
services_dir := $(root_dir)/data/services

###########################################################
# Config
###########################################################

dotenv_paths := "$(root_dir)"

###########################################################
# Help
###########################################################
.PHONY: help

help: ## Shows help
	@printf "\033[33m%s:\033[0m\n" 'Use: make <command> where <command> one of the following'
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "  \033[32m%-18s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

###########################################################
# Initialization
###########################################################
.PHONY: init init-env init-submodules init-npmrc pull

init: init-env init-npmrc init-submodules pull

init-env: ## Initializes .env files
	@echo "$(_SC)$(_OPX)Creating .env files in $(dotenv_paths):$(_NC)"
	@$(foreach dir, $(dotenv_paths), rsync -a -v --ignore-existing $(dir)/.env.template $(dir)/.env; )

init-npmrc: ## Initializes .npmrc
	@if [ ! -f "$(root_dir)/.npmrc" ]; then \
  		echo "$(_SC)$(_OPX)Creating .npmrc$(_NC)"; \
		echo @ista:registry=https://gitlab.cts-uav.org/api/v4/projects/27/packages/npm/ >> .npmrc; \
		echo "//gitlab.cts-uav.org/api/v4/projects/27/packages/npm/:_authToken=<YOUR AUTH TOKEN>" >> .npmrc; \
		echo "$(_EC)$(_OPX)Fill auth token in .npmrc!$(_NC)"; \
	fi

init-submodules: ## Init submodules
	@echo "$(_SC)$(_OPX)Initialising submodules...$(_NC)"
	@git submodule update --init --recursive

pull: ## Pulls container images
	@echo "$(_SC)$(_OPX)Pulling all service images:$(_NC)"
	@$(root_dir)/scripts/pull-service-images.sh $(services_dir)

###########################################################
# Building
###########################################################
.PHONY: build build-docker build-codegen _check_npmrc

build: codegen build-docker ## Generate sources and build all services in Docker

build-tests: _check_npmrc ## Builds Docker images for testing
	@echo "$(_BC)$(_OPX)Building tests Docker image:$(_NC)"
	@docker-compose -f docker-compose.yml -f docker-compose.dev.yml build --no-cache tests-web

build-docker: _check_npmrc ## Build all Docker Compose services
	@echo "$(_BC)$(_OPX)Building all services in Docker:$(_NC)"
	@docker-compose -f docker-compose.yml -f docker-compose.dev.yml build

_check_npmrc:
	@if [ ! -f "$(root_dir)/.npmrc" ]; then \
  		echo "$(_EC)$(_OPX)Configure .npmrc at he root of this project$(_NC)"; \
  		exit -1; \
  	fi

###########################################################
# Running
###########################################################
.PHONY: up down gateway run-core

up: ## Starts all services in Docker
	@echo "$(_RC)$(_OPX)Starting all services in Docker:$(_NC)"
	@docker-compose up

gateway: ## Starts gateway in Docker
	@echo "$(_RC)$(_OPX)Starting dependencies in Docker (background):$(_NC)"
	@docker-compose -f docker-compose.yml -f docker-compose.dev.yml up gateway

run-core: ## Starts `core` service in Docker
	@echo "$(_RC)$(_OPX)Starting 'core' service in Docker:$(_NC)"
	@docker-compose run --rm --service-ports \
		-v /var/run/docker.sock:/var/run/docker.sock \
		-v ./ista.yaml:/etc/ista/ista.yaml \
		core

down: ## Shuts down dockerized application and removes docker resources
	@echo "$(_EC)$(_OPX)Cleaning up Docker:$(_NC)"
	@docker-compose -f docker-compose.yml -f docker-compose.dev.yml down --remove-orphans

###########################################################
# Local development
###########################################################
.PHONY: init-local install _install-go _install-nodejs _check_npmrc build-local codegen docs

init-local: install codegen build-local ## Init development environment on host machine (non-Docker)

install: _install-go _install-nodejs ## Install local dependencies

_install-go:
	@echo "$(_SC)$(_OPX)Installing Go dependencies:$(_NC)"
	@go mod download

_install-nodejs: _check_npmrc
	@echo "$(_SC)$(_OPX)Installing Node.js dependencies:$(_NC)"
	@npm config set strict-ssl false; npm install; npm config set strict-ssl true

build-local: codegen ## Build locally
	@echo "$(_BC)$(_OPX)Building 'ista/cmd/server':$(_NC)"
	go build -o $(dist_dir)/ista ista/cmd

run-core-local: ## Runs `core` service locally
	@echo "$(_RC)$(_OPX)Starting 'core' service locally:$(_NC)"
	@go run "$(server_src_path)"

codegen: clean-codegen ## Generate code
	@echo "$(_BC)$(_OPX)Generating core API gRPC:$(_NC)"
	@podman run -ti --rm --pull always \
     		-v $(root_dir):/src -w /src \
     		registry.cts-uav.org/ista/etc/protoc/golang:latest /bin/bash -c " \
     			cp -r /src/$(proto_path) /registry; \
     			find /registry/ista/modules/core -type f -exec sed -i -E 's/option\sgo_package\s=\s\".+\/modules\/core\/(.+);(.+)\";/option go_package = \"gitlab.cts-uav.org\/ista\/implementation\/golang\/modules\/core\/proto\/\1;\2\";/g' {} +; \
     			mkdir -p /src/pkg/api/proto; \
     			protoc -I /registry \$$(find /registry/ista/modules/core/ -type f -name '*.proto') \
     				--go_out=module=gitlab.cts-uav.org/ista/implementation/golang/modules/core/proto:/src/$(api_grpc_path) \
     				--go-grpc_out=module=gitlab.cts-uav.org/ista/implementation/golang/modules/core/proto:/src/$(api_grpc_path) \
     		"

###########################################################
# Testing
###########################################################
.PHONY: test test-local test-deps

test: ## Run tests
	@echo "$(_RC)$(_OPX)Running web-gRPC tests in Docker...$(_NC)"
	@API_HOST=http://host.docker.internal:17440 docker-compose -f docker-compose.yml -f docker-compose.dev.yml \
		run --rm \
		-v $(web_tests_dir):/opt/tests/web \
		tests-web test

test-local: ## Run tests locally
	@echo "$(_RC)$(_OPX)Running web-gRPC tests locally...$(_NC)"
	@npm run test

test-deps: ## Starts dependencies in Docker
	@echo "$(_RC)$(_OPX)Starting test dependencies in Docker (background):$(_NC)"
	@docker-compose -f docker-compose.yml up -d core

###########################################################
# Cleaning
###########################################################
.PHONY: clean clean-codegen clean-dist reset-env _clear-env

clean: down clean-codegen clean-dist ## Cleans environment

clean-codegen: ## Clean generated code
	@echo "$(_EC)$(_OPX)Removing autogenerated Golang sources...$(_NC)"
	@rm -rf $(api_grpc_dir)

clean-dist: ## Cleans built packages
	@echo "$(_EC)$(_OPX)Removing build...$(_NC)"
	@rm -rf $(dist_dir)

reset-env: _clear-env init-env ## Resets .env files

_clear-env:
	@echo "$(_EC)$(_OPX)Deleting .env files in $(dotenv_paths)...$(_NC)"
	@$(foreach dir, $(dotenv_paths), rm -f $(dir)/.env; )
