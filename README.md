# Ista API

Core API and launcher for web-based MAVLink-compatible GCS

Development
-----------

Initialise repository:

```shell
make init
```

Edit generated `.env` file in the repository root if necessary (see [`.env.template`](.env.template)).
Add your GitLab access token to generated `.npmrc` file.

Build containers and generate source code:

```shell
make build
```

Run all services:

```shell
make up
```

Run only the `core` service (assuming that all its dependencies are running):

```shell
make run-core
```

To tear down all Docker containers:

```shell
make down
```

### Testing

Run test dependencies:

```shell
make test-deps
```

Run tests:

```shell
make test
```

### Local Development

After performing project initialisation. Install dependencies:

```shell
make init-local
```

Run testing dependencies in background (in Docker):

```shell
make test-deps
```

Build application locally:

```shell
make build-local
```

Run `core` service locally:

```shell
make run-core-local
```

To regenerate source code:

```shell
make codegen
```

To run tests locally:

```shell
make test-local
```

### Cleaning Project

The following command will clean up everything but `.env` file:

```shell
make clean
```

To reset `.env` file:

```shell
make reset-env
```

### Repository

We use eager Git ignore policy. Everything is ignored by default and only the necessary things are placed under Git
control. Check [`.gitignore`](.gitignore) for details.
