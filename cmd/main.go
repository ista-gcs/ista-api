package main

import (
	"context"
	"ista/pkg/config"

	"ista/pkg/app"
	_ "ista/pkg/config"
	"ista/pkg/interfaces/core"
	"ista/pkg/logging"
	"ista/pkg/ui"
)

var log = logging.GetLogger("ISTA")

func main() {
	log.Warnf("----------    Starting ISTA ground control station.    ----------")

	ista := app.New()
	go ista.Start(context.Background())
	<-ista.AwaitStatus(core.Started)
	log.Warnf("----------                   Started                   ----------")
	go func() {
		<-ista.AwaitStatus(core.Ready)
		log.Warnf("----------                    Ready                    ----------")
	}()

	if config.Config.Headless {
		log.Warnf("Running ISTA in headless mode.")
		<-ista.AwaitStatus(core.Stopped)
		log.Warnf("----------    ISTA ground control station stopped.     ----------")
	} else {
		// UI should run in the main loop, blocks and exits
		ui.New(ista, log).Run(ista.AwaitStatus(core.Stopped), func() {
			log.Warnf("----------    ISTA ground control station stopped.     ----------")
		})
	}
}
