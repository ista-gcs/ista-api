package api

import (
	"github.com/samber/lo"

	"ista/pkg/api/proto/v1alpha1"
	"ista/pkg/interfaces/core"
)

func convertStatus(status core.Status) v1alpha1.Status {
	switch status {
	case core.Stopped:
		return v1alpha1.Status_Stopped
	case core.Starting:
		return v1alpha1.Status_Starting
	case core.Started:
		return v1alpha1.Status_Started
	case core.Ready:
		return v1alpha1.Status_Ready
	case core.Stopping:
		return v1alpha1.Status_Stopping
	case core.Unknown:
		return v1alpha1.Status_Unknown
	default:
		return v1alpha1.Status_Unknown
	}
}

func convertService(service core.ServiceView) *v1alpha1.Service {
	return &v1alpha1.Service{
		Id:          service.Id(),
		Name:        service.Definition().Name,
		Description: service.Definition().Description,
		Status:      convertStatus(service.Status()),
		Api:         service.Api(),
		Definition:  service.Definition(),
	}
}

func convertServices(services map[string]core.ServiceView) map[string]*v1alpha1.Service {
	return lo.MapValues(services, func(service core.ServiceView, _ string) *v1alpha1.Service {
		return convertService(service)
	})
}
