package api

import (
	"context"
	"fmt"
	"net"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"ista/pkg/api/proto/v1alpha1"
	"ista/pkg/component"
	"ista/pkg/config"
	"ista/pkg/interfaces/core"
)

func New(app core.AppView) core.Api {
	api := &apiImpl{
		app: app,
	}

	api.Component.Init(api, core.ComponentConfig{
		Name:        "Api",
		Description: "ISTA core API",
	}, app, nil)

	return api
}

type apiImpl struct {
	component.Component

	app        core.AppView
	grpcServer *grpc.Server
}

func (api *apiImpl) OnStarting(ctx context.Context) {
	api.grpcServer = api.makeGrpcServer()
	api.serve(ctx)
}

func (api *apiImpl) OnStopping(_ context.Context) {
	if api.grpcServer != nil {
		api.grpcServer.GracefulStop()
		api.grpcServer = nil
	}
}

func (api *apiImpl) makeGrpcServer() *grpc.Server {
	grpcServer := grpc.NewServer(makeGrpcLoggingOptions(api.Log())...)
	reflection.Register(grpcServer)

	v1alpha1.RegisterAppServiceServer(grpcServer, AppServiceServer{app: api.app, log: api.Log()})
	v1alpha1.RegisterServiceServiceServer(grpcServer, ServiceServiceServer{app: api.app, log: api.Log()})

	return grpcServer
}

func (api *apiImpl) serve(ctx context.Context) {
	bindAddr := fmt.Sprintf("%s:%d", config.Config.Host, config.Config.Port)
	listener, lisErr := net.Listen("tcp", bindAddr)
	if lisErr != nil {
		api.Log().Panicf("Can't bind to %v: %v", bindAddr, lisErr)
	}

	go func() {
		if serveErr := api.grpcServer.Serve(listener); serveErr != nil {
			api.Log().Panicf("Can's start gRPC server: %v", serveErr)
		}
	}()
	api.Log().Printf("Started gRPC server at %v", listener.Addr())

	go func() {
		<-ctx.Done()
		api.Log().Printf("xDS gRPC server stopped.")
		api.Stop()
	}()
}
