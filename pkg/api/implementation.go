package api

import (
	"context"
	"errors"
	"fmt"

	"github.com/sirupsen/logrus"

	coreapi "ista/pkg/api/proto/v1alpha1"
	"ista/pkg/interfaces/core"
)

type AppServiceServer struct {
	coreapi.UnimplementedAppServiceServer

	app core.AppView
	log logrus.FieldLogger
}

func (s AppServiceServer) Info(context.Context, *coreapi.AppInfoRequest) (*coreapi.AppInfoResponse, error) {
	return &coreapi.AppInfoResponse{
		Status: convertStatus(s.app.Status()),
	}, nil
}

type ServiceServiceServer struct {
	coreapi.UnimplementedServiceServiceServer

	app core.AppView
	log logrus.FieldLogger
}

func (s ServiceServiceServer) List(context.Context, *coreapi.ListServicesRequest) (*coreapi.ListServicesResponse, error) {
	services := convertServices(s.app.ServiceManagerView().ServiceViews())
	return &coreapi.ListServicesResponse{
		Services: services,
	}, nil
}

func (s ServiceServiceServer) Get(_ context.Context, req *coreapi.GetServiceRequest) (*coreapi.GetServiceResponse, error) {
	if service, present := s.app.ServiceManagerView().ServiceViews()[req.GetId()]; present {
		return &coreapi.GetServiceResponse{
			Service: convertService(service),
		}, nil
	} else {
		return nil, errors.New(fmt.Sprintf("service '%v' is missing", req.GetId()))
	}
}

func (s ServiceServiceServer) Updates(_ *coreapi.ServiceUpdatesRequest, stream coreapi.ServiceService_UpdatesServer) error {
	serviceUpdates := s.app.ServiceManagerView().ServiceUpdates()
	for {
		service, ok := <-serviceUpdates
		if !ok {
			break
		}
		s.log.Printf("Sending service '%v' (ID=%v) update event...", service.Id(), service.Name())
		if err := stream.Send(&coreapi.ServiceUpdatesEvent{
			Event:   coreapi.ServiceUpdatesEventType_ServiceUpdated,
			Service: convertService(service),
		}); err != nil {
			return err
		}
	}
	return nil
}
