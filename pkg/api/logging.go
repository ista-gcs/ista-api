package api

import (
	grpcMiddleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpcLogrus "github.com/grpc-ecosystem/go-grpc-middleware/logging/logrus"
	grpcCtxtags "github.com/grpc-ecosystem/go-grpc-middleware/tags"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"

	"ista/pkg/logging"
)

func makeGrpcLoggingOptions(log logrus.FieldLogger) []grpc.ServerOption {
	// Logrus entry is used, allowing pre-definition of certain fields by the user.
	logrusEntry := log.(*logrus.Entry)
	// Shared options for the logger, with a custom gRPC code to log level function.
	opts := []grpcLogrus.Option{
		grpcLogrus.WithLevels(logging.GrpcLevelFunc),
	}
	// gRPC options for logging
	return []grpc.ServerOption{
		grpcMiddleware.WithUnaryServerChain(
			grpcCtxtags.UnaryServerInterceptor(grpcCtxtags.WithFieldExtractor(grpcCtxtags.CodeGenRequestFieldExtractor)),
			grpcLogrus.UnaryServerInterceptor(logrusEntry, opts...),
		),
		grpcMiddleware.WithStreamServerChain(
			grpcCtxtags.StreamServerInterceptor(grpcCtxtags.WithFieldExtractor(grpcCtxtags.CodeGenRequestFieldExtractor)),
			grpcLogrus.StreamServerInterceptor(logrusEntry, opts...),
		),
	}
}
