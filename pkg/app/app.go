package app

import (
	"context"
	"os"
	"os/signal"
	"syscall"
	"time"

	"ista/pkg/api"
	"ista/pkg/component"
	"ista/pkg/config"
	"ista/pkg/interfaces/core"
	"ista/pkg/services"
)

func New() core.App {
	app := &appImpl{}

	app.Component.Init(app, core.ComponentConfig{
		Name:        "App",
		Description: "ISTA application",
	}, nil, nil)

	app.serviceManager = services.New(app)
	app.api = api.New(app)

	app.Component.Register(
		app.serviceManager,
		app.api,
	)

	go app.stopOnSignal()

	return app
}

type appImpl struct {
	component.Component

	serviceManager core.ServiceManager
	api            core.Api
}

func (app *appImpl) ServiceManagerView() core.ServiceManagerView {
	return app.serviceManager
}

func (app *appImpl) OnStarting(_ context.Context) {}

func (app *appImpl) OnStarted(_ context.Context) {}

func (app *appImpl) OnComponentsReady(_ context.Context) {}

func (app *appImpl) OnReady(_ context.Context) {}

func (app *appImpl) OnStopping(_ context.Context) {}

func (app *appImpl) OnComponentsStopped(_ context.Context) {}

func (app *appImpl) OnStopped(_ context.Context) {}

func (app *appImpl) stopOnSignal() {
	systemSignals := make(chan os.Signal, 1)
	signal.Notify(systemSignals,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)

	s := <-systemSignals

	timeout := config.Config.ShutdownTimeout
	go app.terminateOnTimeout(timeout)

	app.Log().Warnf("Got signal '%v'. Attempts to shut down gracefully (timeout=%s)...", s, timeout)
	app.Stop()
}

func (app *appImpl) terminateOnTimeout(duration time.Duration) {
	time.Sleep(duration)
	app.Terminate()
	app.Log().Fatalf("Can't shut down gracefully. Terminating after %v...", duration)
}
