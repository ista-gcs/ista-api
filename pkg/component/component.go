package component

import (
	"context"
	"ista/pkg/logging"
	"sync"
	"time"

	"github.com/sirupsen/logrus"

	"ista/pkg/interfaces/core"
)

type Callbacks interface {
	OnStarting(ctx context.Context)
	OnStarted(ctx context.Context)
	OnComponentsReady(ctx context.Context)
	OnReady(ctx context.Context)
	OnStopping(ctx context.Context)
	OnComponentsStopped(ctx context.Context)
	OnStopped(ctx context.Context)
}

type Component struct {
	name        string
	description string
	status      core.Status
	log         logrus.FieldLogger

	parent core.ComponentView

	components map[core.Component]struct{}

	statusUpdates map[chan core.Status]struct{}

	mu sync.Mutex

	runningCtx           context.Context
	cancelFunc           context.CancelFunc
	componentsCtx        context.Context
	componentsCancelFunc context.CancelFunc
	stoppingCtx          context.Context
	terminateFunc        context.CancelFunc

	self Callbacks
}

func (c *Component) Init(
	self Callbacks,
	config core.ComponentConfig,
	parent core.ComponentView,
	logger logrus.FieldLogger,
) {
	if logger == nil {
		logger = logging.GetLogger(config.Name)
	}
	if c.status != core.Stopped {
		logger.Panicf("Can't instantiate component which is not stopped!")
	}

	c.self = self
	c.name = config.Name
	c.description = config.Description
	c.log = logger
	c.parent = parent

	c.statusUpdates = make(map[chan core.Status]struct{})
	c.components = make(map[core.Component]struct{})
}

func (c *Component) Log() logrus.FieldLogger {
	return c.log
}

func (c *Component) Name() string {
	return c.name
}

func (c *Component) Description() string {
	return c.description
}

func (c *Component) Status() core.Status {
	return c.status
}

func (c *Component) OnStarting(_ context.Context)          {}
func (c *Component) OnStarted(_ context.Context)           {}
func (c *Component) OnComponentsReady(_ context.Context)   {}
func (c *Component) OnReady(_ context.Context)             {}
func (c *Component) OnStopping(_ context.Context)          {}
func (c *Component) OnComponentsStopped(_ context.Context) {}
func (c *Component) OnStopped(_ context.Context)           {}

// Start starts component and all child components
// Blocks execution until all components started
func (c *Component) Start(ctx context.Context) {
	if c.status != core.Stopped {
		c.log.Debugf("Attempt to start component %v which is already started!", c.name)
		return
	}

	c.runningCtx, c.cancelFunc = context.WithCancel(ctx)
	c.componentsCtx, c.componentsCancelFunc = context.WithCancel(context.Background())
	c.stoppingCtx, c.terminateFunc = context.WithCancel(context.Background())

	c.setStatus(core.Starting)
	c.log.Debugf("%v started", c.name)
	c.self.OnStarting(c.runningCtx)

	// Starting components
	c.log.Debugf("Starting %v components...", c.name)
	for cmp := range c.components {
		cmp.Start(c.componentsCtx)
		c.log.Debugf("Child component %v of %v started.", cmp.Name(), c.name)
	}
	c.log.Debugf("All %v components started.", c.name)

	c.setStatus(core.Started)
	c.log.Debugf("%v started", c.name)
	c.self.OnStarted(c.runningCtx)

	go func() {
		for cmp := range c.components {
			<-cmp.AwaitStatus(core.Ready)
			c.log.Debugf("Child component %v of %v is ready.", cmp.Name(), c.name)
		}
		c.log.Debugf("All %v components ready.", c.name)
		c.self.OnComponentsReady(c.runningCtx)

		c.setStatus(core.Ready)
		c.self.OnReady(c.runningCtx)
		c.log.Debugf("Service %v is ready.", c.name)
	}()

	go func() {
		// Wait for component stop
		<-c.runningCtx.Done()

		c.setStatus(core.Stopping)
		c.self.OnStopping(c.stoppingCtx)

		// Stopping components
		c.log.Debugf("Waiting %v components to stop...", c.name)
		c.componentsCancelFunc()
		for cmp := range c.components {
			c.log.Debugf("Stopping component %v of %v with status: %v.", cmp.Name(), c.name, cmp.Status())
			cmp.Stop()
			<-cmp.AwaitStatus(core.Stopped)
			c.log.Debugf("Component %v of %v stopped.", cmp.Name(), c.name)
		}

		c.log.Debugf("All %v components stopped.", c.name)
		c.self.OnComponentsStopped(c.stoppingCtx)

		c.setStatus(core.Stopped)
		c.log.Debugf("%v stopped.", c.name)
		c.self.OnStopped(c.stoppingCtx)

		defer func() {
			c.mu.Lock()
			defer c.mu.Unlock()
			c.runningCtx, c.cancelFunc = nil, nil
			c.componentsCtx, c.componentsCancelFunc = nil, nil
			c.stoppingCtx, c.terminateFunc = nil, nil
		}()
	}()
}

func (c *Component) Stop() {
	if c.status != core.Started && c.status != core.Ready {
		c.log.Debugf("Attempt to stop component %v which is already stopped!", c.name)
		return
	}

	c.log.Debugf("%v stop requested.", c.name)
	c.cancelFunc()
}

func (c *Component) Terminate() {
	if c.status != core.Started && c.status != core.Ready {
		c.log.Debugf("Attempt to terminate component %v which is already stopped!", c.name)
		return
	}

	c.log.Debugf("%v stop requested.", c.name)
	if c.cancelFunc != nil {
		c.cancelFunc()
	}
	if c.terminateFunc != nil {
		c.terminateFunc()
	}
}

func (c *Component) Register(components ...core.Component) {
	for _, component := range components {
		c.log.Debugf("Registering %v component", component.Name())
		c.register(component)
	}
}

func (c *Component) Statuses(forStatuses ...core.Status) (updates <-chan core.Status, unsubscribe func()) {
	listener := make(chan core.Status)
	enabled := true
	c.statusUpdates[listener] = struct{}{}

	unsubscribe = func() {
		enabled = false
		c.removeStatusUpdates(listener)
	}

	updChan := listener

	// Filter status updates if necessary
	if len(forStatuses) > 0 {
		updChan = make(chan core.Status)
		go func() {
			for enabled {
				received := <-listener
				for _, allowed := range forStatuses {
					if received == allowed {
						updChan <- received
						break
					}
				}
			}
		}()
	}

	return updChan, unsubscribe
}

func (c *Component) OnStatus(handler core.StatusHandler, status core.Status, statuses ...core.Status) {
	current := c.status
	statuses = append(statuses, status)
	_, unsubscribe := c.Statuses(statuses...)

	// Trigger handler immediately if current status is among the list
	for _, st := range statuses {
		if st == current {
			ctx := c.getCtxByStatus(st)
			go func() {
				handler(ctx, st)
				unsubscribe()
			}()
			return
		}
	}

	// Or subscribe to the next status
	c.OnNextStatus(handler, status, statuses...)
}

func (c *Component) OnNextStatus(handler core.StatusHandler, status core.Status, statuses ...core.Status) {
	statuses = append(statuses, status)
	updates, unsubscribe := c.Statuses(statuses...)

	go func() {
		st := <-updates
		ctx := c.getCtxByStatus(st)

		go func() {
			handler(ctx, st)
			unsubscribe()
		}()
	}()
}

func (c *Component) OnStatusChange(handler core.StatusSubscriptionHandler) {
	updates, unsubscribe := c.Statuses()
	subscribed := true

	go func() {
		for subscribed {
			st := <-updates
			ctx := c.getCtxByStatus(st)

			go handler(ctx, st, func() {
				subscribed = false
				unsubscribe()
			})
		}
	}()
}

func (c *Component) AwaitStatus(status core.Status, statuses ...core.Status) <-chan struct{} {
	update := make(chan struct{})
	c.OnStatus(func(ctx context.Context, status core.Status) {
		update <- struct{}{}
		close(update)
	}, status, statuses...)
	return update
}

func (c *Component) AwaitStatusChange(status core.Status, statuses ...core.Status) <-chan struct{} {
	update := make(chan struct{})
	c.OnNextStatus(func(ctx context.Context, status core.Status) {
		update <- struct{}{}
		close(update)
	}, status, statuses...)
	return update
}

func (c *Component) InContext(handler func(ctx context.Context, status core.Status)) {
	ctx := c.getCtxByStatus(c.status)
	handler(ctx, c.status)
}

func (c *Component) inStatus(status core.Status, statuses ...core.Status) bool {
	statuses = append(statuses, status)
	for _, st := range statuses {
		if st == c.status {
			return true
		}
	}
	return false
}

func (c *Component) getCtxByStatus(status core.Status) context.Context {
	ctx := c.runningCtx
	if status == core.Stopping || status == core.Stopped {
		ctx = c.stoppingCtx
	}
	return ctx
}

func (c *Component) register(component core.Component) {
	if _, present := c.components[component]; !present {
		c.components[component] = struct{}{}

		if c.status != core.Stopped && c.status != core.Stopping {
			c.OnStatus(func(ctx context.Context, status core.Status) {
				component.Start(ctx)
			}, core.Started, core.Ready)
		}
	} else {
		c.log.Warnf("Attempt to register again %v component to %v", component.Name(), c.name)
	}
}

func (c *Component) removeStatusUpdates(updates chan core.Status) {
	c.mu.Lock()
	defer c.mu.Unlock()

	delete(c.statusUpdates, updates)
	defer func() {
		// TODO: This is a very dirty hack. We need to review the whole component solution.
		time.Sleep(time.Second)
		close(updates)
	}()
}

func isCorrectStatusSequence(old core.Status, new core.Status) bool {
	if old == core.Stopping && new == core.Ready {
		return false
	}
	if old == core.Stopping && new == core.Started {
		return false
	}
	if old == core.Stopping && new == core.Starting {
		return false
	}
	return true
}

func (c *Component) setStatus(newStatus core.Status) {
	if !isCorrectStatusSequence(c.status, newStatus) {
		c.log.Warnf("Rejecting %v -> %v status change in %v", c.status, newStatus, c.name)
		return
	}

	c.log.Debugf("%v status change: %v -> %v", c.name, c.status, newStatus)
	c.status = newStatus
	c.updateStatusReceivers(newStatus)
}

func (c *Component) updateStatusReceivers(status core.Status) {
	c.mu.Lock()
	defer c.mu.Unlock()

	for updated := range c.statusUpdates {
		updated := updated
		go func() {
			updated <- status
		}()
	}
}
