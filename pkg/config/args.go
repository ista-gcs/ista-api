package config

import (
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

func defineCLiArgs(config *viper.Viper) {
	// At this moment all default variables are set

	pflag.StringP("log_level", "l", config.GetString("log_level"), "Log level")
	pflag.DurationP("shutdown_timeout", "t", config.GetDuration("shutdown_timeout"),
		"Shutdown timeout duration when system signal received. After this timeout application will try to terminate forcefully")
	pflag.StringP("host", "h", config.GetString("host"), "ISTA web API host")
	pflag.Uint32P("port", "p", config.GetUint32("port"), "ISTA web API port")
	pflag.BoolP("headless", "s", config.GetBool("headless"), "Run ISTA in headless mode")

	pflag.Parse()
	_ = config.BindPFlags(pflag.CommandLine)
}
