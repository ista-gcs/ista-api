package config

import (
	"log"
	"os"
	"path/filepath"
	"time"

	"github.com/samber/lo"
	"github.com/spf13/viper"
)

type AppConfig struct {
	LogLevel        string
	ShutdownTimeout time.Duration
	Host            string
	Port            uint32
	Headless        bool
	ServicePrefix   string
	ApiRoot         string
	SkipServices    map[string]struct{}

	Paths       PathsConfig
	Conventions ConventionsConfig
}

type PathsConfig struct {
	WorkDir            string
	UserHomeDir        string
	ServiceDefinitions string
}

type ConventionsConfig struct {
	LocalHost    string
	InternalHost string
	ApiPort      uint32
}

func getConfig(config *viper.Viper) *AppConfig {
	workDir, err := os.Getwd()
	if err != nil {
		log.Panicf("Can't resolve working directory: %v", err)
	}
	userHomeDir, err := os.UserHomeDir()
	if err != nil {
		log.Panicf("Can't obtain user home directory!")
	}

	conf := &AppConfig{
		LogLevel:        config.GetString("log_level"),
		ShutdownTimeout: config.GetDuration("shutdown_timeout"),
		Host:            config.GetString("host"),
		Port:            config.GetUint32("port"),
		Headless:        config.GetBool("headless"),
		ServicePrefix:   config.GetString("service_prefix"),
		ApiRoot:         config.GetString("api_root"),
		SkipServices: lo.Associate(config.GetStringSlice("skip_services"), func(t string) (string, struct{}) {
			return t, struct{}{}
		}),
		Paths: PathsConfig{
			WorkDir:            workDir,
			UserHomeDir:        userHomeDir,
			ServiceDefinitions: filepath.Join(workDir, "data", "services"),
		},
		Conventions: ConventionsConfig{
			InternalHost: "host.containers.internal",
			LocalHost:    "127.0.0.1",
			ApiPort:      config.GetUint32("api_port"),
		},
	}
	return conf
}
