package config

import (
	"time"

	"github.com/spf13/viper"
)

func setDefaults(config *viper.Viper) {
	config.SetTypeByDefaultValue(true)

	config.SetDefault("log_level", "info")
	config.SetDefault("shutdown_timeout", 3*time.Second)
	config.SetDefault("host", "0.0.0.0")
	config.SetDefault("port", uint32(17440))
	config.SetDefault("headless", false)
	config.SetDefault("service_prefix", "ista")
	config.SetDefault("api_root", "/api/v1/")
	config.SetDefault("api_port", uint32(17440))
	config.SetDefault("skip_services", []string{})
}
