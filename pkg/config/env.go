package config

import (
	"github.com/spf13/viper"
)

func defineEnvBindings(config *viper.Viper) {
	config.AutomaticEnv()
	config.SetEnvPrefix("ista")
}
