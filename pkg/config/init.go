package config

import (
	"fmt"
	"github.com/spf13/viper"
	"os"
	"path/filepath"
)

var Config *AppConfig

func getProgramDir() string {
	ex, err := os.Executable()
	if err != nil {
		panic(err)
	}
	return filepath.Dir(ex)
}

func setConfigPaths(config *viper.Viper) {
	paths := []string{
		"/etc/ista/",
		"$HOME/.ista",
		".",
		getProgramDir(),
	}

	for _, path := range paths {
		config.AddConfigPath(path)
	}
}

func init() {
	config := viper.New()

	setDefaults(config)

	config.SetConfigName("ista")
	config.SetConfigType("yaml")
	setConfigPaths(config)

	defineEnvBindings(config)
	defineCLiArgs(config)

	if err := config.ReadInConfig(); err != nil {
		panic(fmt.Errorf("fatal error config file: %w", err))
	}

	Config = getConfig(config)
}
