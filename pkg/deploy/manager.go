package deploy

import (
	"context"
	"ista/pkg/api/proto/v1alpha1"

	"ista/pkg/component"
	"ista/pkg/interfaces/core"
	"ista/pkg/podman"
)

func New(parent core.ServiceManagerView) core.DeploymentManager {
	manager := &DeploymentManager{}

	manager.Component.Init(manager, core.ComponentConfig{
		Name:        "DeploymentManager",
		Description: "Deploys services and manages their lifecycle.",
	}, parent, nil)

	manager.podmanManager = podman.NewManager(manager)
	manager.Component.Register(manager.podmanManager)

	return manager
}

type DeploymentManager struct {
	component.Component

	podmanManager core.PodmanManager
}

func (dm *DeploymentManager) Deploy(_ context.Context, service core.ServiceView) error {
	if service.Definition().Deployment.Type == v1alpha1.DeploymentType_Podman {
		return dm.podmanManager.Deploy(service)
	}
	return nil
}

func (dm *DeploymentManager) Remove(service core.ServiceView) error {
	if service.Definition().Deployment.Type == v1alpha1.DeploymentType_Podman {
		return dm.podmanManager.Remove(service)
	}
	return nil
}
