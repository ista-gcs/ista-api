package core

type ApiType int32

const (
	Unavailable         = 0
	GRPC        ApiType = 1
)

func (serviceType ApiType) String() string {
	switch serviceType {
	case GRPC:
		return "gRPC"
	case Unavailable:
		return "Unavailable"
	default:
		return "UNKNOWN"
	}
}
