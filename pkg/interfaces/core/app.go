package core

type AppView interface {
	ComponentView

	ServiceManagerView() ServiceManagerView
}

type App interface {
	Component
	ComponentControl
	AppView
}
