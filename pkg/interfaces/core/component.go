package core

import (
	"context"
)

type StatusHandler = func(ctx context.Context, status Status)
type StatusSubscriptionHandler = func(ctx context.Context, status Status, unsubscribe func())

type SubscribableComponent interface {
	Statuses(forStatuses ...Status) (updates <-chan Status, unsubscribe func())
	OnStatus(handler StatusHandler, status Status, statuses ...Status)
	OnNextStatus(handler StatusHandler, status Status, statuses ...Status)
	OnStatusChange(handler StatusSubscriptionHandler)
	AwaitStatus(status Status, statuses ...Status) <-chan struct{}
	AwaitStatusChange(status Status, statuses ...Status) <-chan struct{}

	InContext(handler func(ctx context.Context, status Status))
}

type ComponentView interface {
	SubscribableComponent

	Name() string
	Description() string
	Status() Status
}

type ComponentControl interface {
	Start(ctx context.Context)
	Stop()
	Terminate()
}

type Component interface {
	ComponentView
	ComponentControl
}

type ComponentConfig struct {
	Name        string
	Description string
}
