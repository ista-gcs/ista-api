package core

import "context"

type DeploymentManagerView interface {
	ComponentView

	Deploy(ctx context.Context, service ServiceView) error
	Remove(service ServiceView) error
}

type DeploymentManager interface {
	DeploymentManagerView
	Component
}
