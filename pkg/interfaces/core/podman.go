package core

type PodmanManager interface {
	Component

	Deploy(service ServiceView) error
	Remove(service ServiceView) error
}

type Pod struct {
	Id   string
	Name string
}

type PodConfig struct {
	Name         string
	TemplatePath string
}

type PodTemplateValues struct {
	PodName          string
	RemoteSocketPath string
}
