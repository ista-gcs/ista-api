package core

import "ista/pkg/api/proto/v1alpha1"

type ServiceView interface {
	ComponentView

	Id() string
	Definition() *v1alpha1.ServiceDefinition
	Api() *v1alpha1.ServiceApi
}

type Service interface {
	ServiceView
	ComponentControl
}
