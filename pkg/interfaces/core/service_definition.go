package core

import "ista/pkg/api/proto/v1alpha1"

type ServiceDefinitionsLoader interface {
	LoadAll() (map[string]*v1alpha1.ServiceDefinition, error)
	LoadById(id string) (*v1alpha1.ServiceDefinition, error)
}
