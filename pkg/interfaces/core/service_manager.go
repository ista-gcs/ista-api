package core

type ServiceManagerView interface {
	ComponentView

	ServiceViews() map[string]ServiceView
	DeploymentManagerView() DeploymentManagerView

	ServiceUpdates() <-chan ServiceView
}

type ServiceManager interface {
	ServiceManagerView
	Component

	Services() map[string]Service
	DeploymentManager() DeploymentManager
}
