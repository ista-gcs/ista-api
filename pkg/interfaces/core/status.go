package core

type Status int32

const (
	Stopped  Status = 0
	Starting        = 1
	Started         = 2
	Ready           = 3
	Stopping        = 4
	Unknown         = 5
)

func (status Status) String() string {
	switch status {
	case Stopped:
		return "Stopped"
	case Starting:
		return "Starting"
	case Started:
		return "Started"
	case Ready:
		return "Ready"
	case Stopping:
		return "Stopping"
	case Unknown:
		return "Unknown"
	default:
		return "UNKNOWN"
	}
}
