package logging

import (
	"fmt"

	logPrefixed "github.com/chappjc/logrus-prefix"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc/codes"

	"ista/pkg/config"
	"strings"
)

const (
	rootLoggerPrefix = "ROOT"
)

var (
	rootLogger = func() *logrus.Logger {
		logger := logrus.New()

		logger.SetReportCaller(true)
		logger.Formatter = &logPrefixed.TextFormatter{
			FullTimestamp: true,
		}
		logger.SetLevel(levelFromString(config.Config.LogLevel))
		logger.Debug("Root logger configured.")

		return logger
	}()
)

func GetLogger(prefix string) logrus.FieldLogger {
	if prefix == "" {
		prefix = rootLoggerPrefix
	}

	return rootLogger.WithFields(logrus.Fields{
		"prefix": fmt.Sprintf("[%s]", prefix),
	})
}

func GrpcLevelFunc(code codes.Code) logrus.Level {
	if code == codes.OK {
		return logrus.DebugLevel
	}
	return logrus.ErrorLevel
}

func levelFromString(level string) logrus.Level {
	const defaultLevel = "info"
	var logLevel, ok = logrus.ParseLevel(strings.ToLower(level))

	if ok != nil {
		logLevel = levelFromString(defaultLevel)
	}

	return logLevel
}
