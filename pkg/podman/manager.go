package podman

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"ista/pkg/config"
	"strings"
	"text/template"

	"github.com/containers/podman/v4/pkg/bindings"
	"github.com/containers/podman/v4/pkg/bindings/play"
	"github.com/containers/podman/v4/pkg/bindings/pods"

	"ista/pkg/component"
	"ista/pkg/interfaces/core"
)

func NewManager(
	parent core.DeploymentManagerView,
) core.PodmanManager {
	dm := &managerImpl{
		pods: map[string]core.Pod{},
	}

	dm.Component.Init(dm, core.ComponentConfig{
		Name:        "PodmanManager",
		Description: "Manages Pod-based services.",
	}, parent, nil)

	return dm
}

type managerImpl struct {
	component.Component

	connCtx          context.Context
	remoteSocketPath *string
	connCancelFunc   context.CancelFunc

	pods map[string]core.Pod
}

func (pm *managerImpl) OnStarting(_ context.Context) {
	ctx, cancelFunc := context.WithCancel(context.Background())
	connCtx, remoteSocketPath, err := pm.getConnection(ctx)
	if err != nil {
		cancelFunc()
		pm.Log().Panicf("Can't establish connection to Podman: %v", err)
	}
	pm.connCtx = connCtx
	pm.connCancelFunc = cancelFunc
	pm.remoteSocketPath = &remoteSocketPath
}

func (pm *managerImpl) OnStopped(ctx context.Context) {
	// Stop all activities withing context
	<-ctx.Done()
	pm.connCancelFunc()

	// Reset state
	pm.connCancelFunc = nil
	pm.connCtx = nil
	pm.remoteSocketPath = nil
}

func (pm *managerImpl) Deploy(service core.ServiceView) error {
	if pm.connCtx == nil {
		return errors.New(fmt.Sprintf("can't deploy when Podman Manager is not initialised. "+
			"Service status: %v", pm.Status()))
	}

	podfile, err := pm.generatePod(service)
	if err != nil {
		return err
	}

	forceRemove := true
	_, _ = pods.Remove(pm.connCtx, service.Name(), &pods.RemoveOptions{Force: &forceRemove})

	rep, err := play.KubeWithBody(pm.connCtx, strings.NewReader(*podfile), &play.KubeOptions{})
	if err != nil {
		return err
	}

	for _, pod := range rep.Pods {
		pm.Log().Debugf("Started pod '%v': %v", service.Name(), pod.ID)
	}

	return nil
}

func (pm *managerImpl) Remove(service core.ServiceView) error {
	if pm.connCtx == nil {
		return errors.New(fmt.Sprintf("can't deploy when Podman Manager is not initialised. "+
			"Service status: %v", pm.Status()))
	}

	forceRemove := true
	rep, err := pods.Remove(pm.connCtx, service.Name(), &pods.RemoveOptions{Force: &forceRemove})

	if err != nil {
		pm.Log().Debugf("Can't remove pod '%v': %v", service.Name(), err)
		return err
	}

	pm.Log().Debugf("Removed pod '%v' (ID=%v)", service.Name(), rep.Id)
	return nil
}

func (pm *managerImpl) getConnection(ctx context.Context) (connCtx context.Context, remoteSocketPath string, err error) {
	socket, sockErr := getSocket()
	if sockErr != nil {
		return connCtx, remoteSocketPath, sockErr
	}

	connCtx, connErr := bindings.NewConnection(ctx, socket)
	if connErr != nil {
		return connCtx, remoteSocketPath, connErr
	}

	remoteSocketPath, err = getRemoteSocketPath()

	return connCtx, remoteSocketPath, err
}

func (pm *managerImpl) generatePod(service core.ServiceView) (*string, error) {
	tmpl, err := template.New(service.Id()).Parse(*service.Definition().Deployment.Pod.Template)
	if err != nil {
		return nil, err
	}

	var out bytes.Buffer
	err = tmpl.Execute(&out, PodTemplateArguments{
		PodName:     service.Name(),
		Definition:  service.Definition(),
		Conventions: config.Config.Conventions,
	})
	if err != nil {
		return nil, err
	}

	podContent := out.String()
	return &podContent, nil
}
