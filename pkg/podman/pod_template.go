package podman

import (
	"ista/pkg/api/proto/v1alpha1"
	"ista/pkg/config"
)

type PodTemplateArguments struct {
	PodName     string
	Definition  *v1alpha1.ServiceDefinition
	Conventions config.ConventionsConfig
}
