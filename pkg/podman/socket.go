//go:build !darwin

package podman

import "os"

func getSocket() (socket string, err error) {
	sock_dir := os.Getenv("XDG_RUNTIME_DIR")
	socket = "unix:" + sock_dir + "/podman/podman.sock"
	return socket, nil
}

func getRemoteSocketPath() (remoteSocketPath string, err error) {
	info, infoErr := registry.ContainerEngine().Info(registry.Context())
	if infoErr != nil {
		err = infoErr
		return remoteSocketPath, err
	}

	if !info.Host.RemoteSocket.Exists {
		err = infoErr
		connCtx = nil
		return remoteSocketPath, errors.New(fmt.Sprintf("Podman remote socket is not available!"))
	} else {
		remoteSocketPath = info.Host.RemoteSocket.Path
	}

	return remoteSocketPath, err
}
