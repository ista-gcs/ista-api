//go:build darwin

package podman

import (
	"fmt"
	"path"

	"ista/pkg/config"
)

func getSocket() (socket string, err error) {
	sockPath := path.Join(config.Config.Paths.UserHomeDir, ".local/share/containers/podman/machine/podman.sock")
	return fmt.Sprintf("unix:%s", sockPath), nil
}

func getRemoteSocketPath() (remoteSocketPath string, err error) {
	return "/run/user/501/podman/podman.sock", nil
}
