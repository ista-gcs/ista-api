package services

import (
	"context"
	"ista/pkg/api/proto/v1alpha1"
	"ista/pkg/component"
	"ista/pkg/config"
	"ista/pkg/deploy"
	"ista/pkg/interfaces/core"
)

func New(app core.App) core.ServiceManager {
	manager := &serviceManager{}

	manager.Component.Init(manager, core.ComponentConfig{
		Name:        "ServiceManager",
		Description: "Manages services deployment and controls API.",
	}, app, nil)

	manager.definitions = make(map[string]*v1alpha1.ServiceDefinition)
	manager.services = make(map[string]core.Service)
	manager.serviceUpdates = make(map[chan core.ServiceView]struct{})

	manager.deployManager = deploy.New(manager)
	manager.Component.Register(manager.deployManager)

	return manager
}

type serviceManager struct {
	component.Component

	definitions map[string]*v1alpha1.ServiceDefinition
	services    map[string]core.Service

	deployManager core.DeploymentManager

	serviceUpdates map[chan core.ServiceView]struct{}
}

func (sm *serviceManager) OnStarting(_ context.Context) {
	sm.loadAllDefinitions()
}

func (sm *serviceManager) OnStarted(_ context.Context) {
	sm.registerAllServices()
}

func (sm *serviceManager) OnComponentsReady(ctx context.Context) {
	for _, srvs := range sm.services {
		srvs.Start(ctx)
	}
	sm.Log().Printf("Deployed all services.")
}

func (sm *serviceManager) ServiceViews() map[string]core.ServiceView {
	views := make(map[string]core.ServiceView)
	for id, srv := range sm.services {
		views[id] = srv
	}
	return views
}

func (sm *serviceManager) ServiceUpdates() <-chan core.ServiceView {
	updates := make(chan core.ServiceView)
	sm.serviceUpdates[updates] = struct{}{}
	return updates
}

func (sm *serviceManager) DeploymentManagerView() core.DeploymentManagerView {
	return sm.DeploymentManager()
}

func (sm *serviceManager) Services() map[string]core.Service {
	return sm.services
}

func (sm *serviceManager) DeploymentManager() core.DeploymentManager {
	return sm.deployManager
}

func (sm *serviceManager) loadAllDefinitions() {
	sm.Log().Debugf("Loading service definitions...")

	loader := NewServiceDefinitionsLoader(config.Config.Paths.ServiceDefinitions, sm.Log())
	definitions, err := loader.LoadAll()
	if err != nil {
		sm.Log().Panicf("Can't load service definitions: %v", err)
	}

	for id, definition := range definitions {
		sm.definitions[id] = definition
	}

	sm.Log().Printf("Loaded all service definitions.")
}

func (sm *serviceManager) registerAllServices() {
	for id, definition := range sm.definitions {
		sm.registerService(id, definition)
	}
}

func (sm *serviceManager) registerService(id string, definition *v1alpha1.ServiceDefinition) {
	srvs := NewService(id, definition, sm)
	sm.services[id] = srvs
	sm.Register(srvs)
	sm.Log().Printf("Registered service '%v' (ID=%v).", srvs.Name(), srvs.Id())

	sm.reportServiceUpdate(srvs)
	srvs.OnStatusChange(func(ctx context.Context, status core.Status, unsubscribe func()) {
		sm.Log().Debugf("Reporting service '%v' (ID=%v) status: %v.", srvs.Name(), srvs.Id(), status)
		sm.reportServiceUpdate(srvs)
		// TODO: Investigate potential memory leak since we don't call unsubscribe function
	})

	if sm.Status() == core.Ready {
		sm.Log().Debugf("Service manager state is %s (active). Starting service '%v' (ID=%v)...",
			sm.Status(), srvs.Name(), srvs.Id())

		sm.InContext(func(ctx context.Context, status core.Status) {
			srvs.Start(ctx)
		})
	}
}

func (sm *serviceManager) reportServiceUpdate(srvs core.ServiceView) {
	for update := range sm.serviceUpdates {
		update <- srvs
	}
}
