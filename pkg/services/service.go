package services

import (
	"context"
	"fmt"
	"strings"

	"github.com/samber/lo"

	"ista/pkg/api/proto/v1alpha1"
	"ista/pkg/component"
	"ista/pkg/config"
	"ista/pkg/interfaces/core"
)

func NewService(id string, definition *v1alpha1.ServiceDefinition, sm core.ServiceManagerView) core.Service {
	srvs := &service{
		id:         id,
		definition: definition,
		sm:         sm,
	}

	srvs.Component.Init(srvs, core.ComponentConfig{
		Name:        srvs.definition.Name,
		Description: srvs.definition.Description,
	}, sm, nil)

	return srvs
}

type service struct {
	component.Component

	id string

	sm         core.ServiceManagerView
	definition *v1alpha1.ServiceDefinition
}

func (s *service) Id() string {
	return s.id
}

func (s *service) Name() string {
	return fmt.Sprintf("%v-%v", config.Config.ServicePrefix, s.id)
}

func (s *service) Api() *v1alpha1.ServiceApi {
	ports := lo.Associate(s.definition.Deployment.Ports, func(p *v1alpha1.Port) (string, *v1alpha1.Port) {
		return p.Id, p
	})

	api := &v1alpha1.ServiceApi{
		Endpoints: []*v1alpha1.ServiceApiEndpoint{},
	}

	lo.ForEach(
		s.definition.Api.Endpoints,
		func(endpointDefinition *v1alpha1.EndpointDefinition, _ int) {
			for _, port := range s.definition.Deployment.Ports {
				if port.Id == endpointDefinition.Id {
					break
				}
			}
			port, present := ports[endpointDefinition.Id]
			if !present {
				s.Log().Panicf("Can't find port for endpoint '%v'!", endpointDefinition.Id)
			}

			switch endpointDefinition.Type {
			case v1alpha1.EndpointType_GrpcServer:
				webRootPath := fmt.Sprintf("%s%s%s", config.Config.ApiRoot, s.Id(), endpointDefinition.Path)
				// Remove trailing "/" from the path
				if strings.HasSuffix(webRootPath, "/") && len(webRootPath) != 1 {
					webRootPath = webRootPath[:len(webRootPath)-1]
				}
				webEndpoint := &v1alpha1.ServiceApiEndpoint{
					Id:   endpointDefinition.Id,
					Type: v1alpha1.ApiType_WebGrpc,
					Host: config.Config.Conventions.LocalHost,
					Port: config.Config.Conventions.ApiPort,
					Path: webRootPath,
				}
				api.Endpoints = append(api.Endpoints, webEndpoint)

				serviceEndpoint := &v1alpha1.ServiceApiEndpoint{
					Id:   endpointDefinition.Id,
					Type: v1alpha1.ApiType_Grpc,
					Port: port.Value,
					Path: endpointDefinition.Path,
				}
				switch s.definition.Deployment.Type {
				case v1alpha1.DeploymentType_Local:
					serviceEndpoint.Host = config.Config.Conventions.InternalHost
				case v1alpha1.DeploymentType_Podman:
					serviceEndpoint.Host = s.Name()
				}
				api.Endpoints = append(api.Endpoints, serviceEndpoint)
			}

		},
	)

	return api
}

func (s *service) Definition() *v1alpha1.ServiceDefinition {
	return s.definition
}

func (s *service) OnStarting(ctx context.Context) {
	s.Log().Debugf("Deploying service '%v' (ID=%v).", s.Name(), s.Id())

	err := s.sm.DeploymentManagerView().Deploy(ctx, s)
	if err != nil {
		if s.definition.Essential {
			s.Log().Panicf("Can't start essential service '%v' (ID=%v): %v", s.Name(), s.Id(), err)
		}
		s.Log().Errorf("Can't start service '%v' (ID=%v): %v", s.Name(), s.Id(), err)
		s.Stop()
	}

	s.Log().Printf("Service '%v' (ID=%v) deployed.", s.Name(), s.Id())
}

func (s *service) OnStarted(_ context.Context) {
	s.Log().Debugf("Awaiting API gateway configuration for service '%v' (ID=%v).", s.Name(), s.Id())
	s.Log().Printf("Service '%v' (ID=%v) API is ready.", s.Name(), s.Id())
}

func (s *service) OnStopping(_ context.Context) {
	s.Log().Debugf("Removing service '%v' (ID=%v).", s.Name(), s.Id())

	err := s.sm.DeploymentManagerView().Remove(s)
	if err != nil {
		s.Log().Errorf("Can't remove service '%v' (ID=%v): %v", s.Name(), s.Id(), err)
	}

	s.Log().Printf("Service '%v' (ID=%v) removed.", s.Name(), s.Id())
}
