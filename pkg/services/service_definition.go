package services

import (
	"errors"
	"fmt"
	"io/ioutil"
	"ista/pkg/config"
	"path/filepath"

	"github.com/ghodss/yaml"
	"github.com/sirupsen/logrus"
	"google.golang.org/protobuf/encoding/protojson"

	"ista/pkg/api/proto/v1alpha1"
	"ista/pkg/interfaces/core"
)

func NewServiceDefinitionsLoader(path string, log logrus.FieldLogger) core.ServiceDefinitionsLoader {
	loader := &sdLoader{
		path: path,
		log:  log,
	}
	return loader
}

type sdLoader struct {
	path string
	log  logrus.FieldLogger
}

func (loader *sdLoader) LoadAll() (map[string]*v1alpha1.ServiceDefinition, error) {
	definitions := make(map[string]*v1alpha1.ServiceDefinition)

	files, err := ioutil.ReadDir(config.Config.Paths.ServiceDefinitions)
	if err != nil {
		loader.log.Errorf("Can't list service definitions from '%v': %v", config.Config.Paths.ServiceDefinitions, err)
		return definitions, err
	}
	for _, f := range files {
		if f.IsDir() {
			loader.log.Debugf("Found service definition in '%v'.", f.Name())

			definitionId := f.Name()

			definition, err := loader.LoadById(f.Name())
			if err != nil {
				loader.log.Errorf("Can't load service definition '%v': %v", definitionId, err)
				return definitions, err
			}

			loader.log.Printf("Found definition for service '%v' (ID=%v)", definition.Name, definitionId)

			if _, present := config.Config.SkipServices[definitionId]; present {
				loader.log.Warnf("Definition for service '%v' (ID=%v) won't be registered (excluded in config).",
					definition.Name, definitionId)
				continue
			}
			definitions[definitionId] = definition
		}
	}

	return definitions, nil
}

func (loader *sdLoader) LoadById(id string) (*v1alpha1.ServiceDefinition, error) {
	defPath := filepath.Join(loader.path, id, "definition.yml")

	yamlBytes, err := ioutil.ReadFile(defPath)
	if err != nil {
		return nil, err
	}
	loader.log.Debug("Loaded service definition config '%v' from %v", id, defPath)

	definition := &v1alpha1.ServiceDefinition{}
	jsonBytes, err := yaml.YAMLToJSON(yamlBytes)
	if err != nil {
		return nil, err
	}
	if err := protojson.Unmarshal(jsonBytes, definition); err != nil {
		return nil, err
	}
	loader.log.Debug("Loaded service definition '%v' (ID=%v).", definition.Name, id)

	if id != definition.Id {
		return nil, errors.New(fmt.Sprintf("service ID should match directory name "+
			"but '%v' != '%v'!", definition.Id, id))
	}

	if definition.Deployment.Type == v1alpha1.DeploymentType_Podman {
		if definition.Deployment.Pod == nil {
			definition.Deployment.Pod = &v1alpha1.Pod{}
		}

		if definition.Deployment.Pod.Template == nil {
			tmplPath := filepath.Join(loader.path, id, "pod.template.yml")

			tmplBytes, err := ioutil.ReadFile(tmplPath)
			if err != nil {
				loader.log.Errorf("Service definition of '%v' (ID=%v) has a Podman deployment "+
					"but pod template is unavailable: %v", definition.Name, id, err)
				return definition, err
			}

			tmplContent := string(tmplBytes)
			definition.Deployment.Pod.Template = &tmplContent

			loader.log.Debug("Loaded pod template for '%v' (ID=%v) service definition.", definition.Name, id)
		}
	}

	return definition, nil
}
