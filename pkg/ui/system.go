package ui

import (
	"fyne.io/systray"
	"fyne.io/systray/example/icon"
	"github.com/pkg/browser"
	"github.com/sirupsen/logrus"

	"ista/pkg/interfaces/core"
)

type System struct {
	app     core.App
	log     logrus.FieldLogger
	started bool
}

func New(app core.App, log logrus.FieldLogger) *System {
	sys := System{
		app: app,
		log: log,
	}
	return &sys
}

func (sys *System) Run(exited <-chan struct{}, onStop func()) {
	go func() {
		<-exited
		sys.log.Debugf("Stopping system tray.")
		systray.Quit()
	}()

	sys.log.Debugf("Starting system tray.")
	sys.started = true
	systray.Run(sys.onReady, onStop)
}

func appStatus(status core.Status) string {
	return "Status: " + status.String()
}

func (sys *System) onReady() {
	sys.log.Debugf("Configuring system tray.")

	systray.SetIcon(icon.Data)
	systray.SetTooltip("ISTA Ground Control")

	mOpen := systray.AddMenuItem("Open", "")
	mOpen.Hide()
	mStatus := systray.AddMenuItem(appStatus(sys.app.Status()), "")
	mStatus.Disable()
	systray.AddSeparator()
	mQuit := systray.AddMenuItem("Quit", "")

	go func() {
		if sys.app.Status() == core.Ready {
			mOpen.Show()
		}
		statusUpdated, unsubscribe := sys.app.Statuses()
		for sys.started {
			select {
			case <-mQuit.ClickedCh:
				sys.log.Debugf("Application stop requested by user (tray).")
				unsubscribe()
				go sys.app.Stop()
			case <-mOpen.ClickedCh:
				if err := browser.OpenURL("https://google.com"); err != nil {
					sys.log.Warnf("Can't open application in browser: %v", err)
				}
			case status := <-statusUpdated:
				if status == core.Ready {
					mOpen.Show()
				} else {
					mOpen.Hide()
				}
				mStatus.SetTitle(appStatus(status))
			}
		}
	}()
}
