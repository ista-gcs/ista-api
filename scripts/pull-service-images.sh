#!/usr/bin/env bash

set -e

services_dir=${1-.}

declare -a service_dirs=($(find "${services_dir}" -maxdepth 1 -mindepth 1 -type d -print))
for path in "${service_dirs[@]}"
do
  if [ -f "${path}/containers" ]; then
    echo "Pulling containers for '$(basename "${path}")' service:"
    while read -r image; do
      echo "Pulling ${image}..."
      podman pull "${image}"
    done < "${path}/containers"
  fi
done
