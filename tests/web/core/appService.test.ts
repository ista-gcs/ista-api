import { AppServicePromiseClient } from '@ista/gen-mod-core/src/v1alpha1/app_grpc_web_pb';
import {
    AppInfoRequest,
} from '@ista/gen-mod-core/src/v1alpha1/app_pb';
import {
    Status,
} from '@ista/gen-mod-core/src/v1alpha1/common_pb';
import {
    API_CORE_URL
} from "../settings";

beforeAll(() => {
    require('browser-env')();
});

describe('CoreService', () => {
    it('app should be ready', async () => {
        const service = new AppServicePromiseClient(API_CORE_URL);
        const response = await service.info(new AppInfoRequest());

        expect(response.getStatus()).toEqual(Status.READY);
    });
});
