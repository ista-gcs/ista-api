import { ServiceServicePromiseClient } from '@ista/gen-mod-core/src/v1alpha1/service_grpc_web_pb';
import {
    ListServicesRequest,
    GetServiceRequest,
} from '@ista/gen-mod-core/src/v1alpha1/service_pb';
import {
    Status,
} from '@ista/gen-mod-core/src/v1alpha1/common_pb';
import {
    ServiceApiEndpoint,
    ApiType,
} from '@ista/gen-mod-core/src/v1alpha1/service_messages_pb';
import {
    API_HOST,
    API_CORE_URL
} from "../settings";

beforeAll(() => {
    require('browser-env')();
});

describe('ServiceService', () => {
    it('should list services', async () => {
        const service = new ServiceServicePromiseClient(API_CORE_URL);
        const response = await service.list(new ListServicesRequest());

        expect((new Map(response.toObject().servicesMap)).keys()).toContain("core");
    });

    it('should return service by ID', async () => {
        const service = new ServiceServicePromiseClient(API_CORE_URL);
        const request = new GetServiceRequest().setId("core");

        const response = await service.get(request);

        expect(response.getService()?.getId()).toEqual("core");
        expect(response.getService()?.getStatus()).toEqual(Status.READY);
        expect(response.getService()?.getDefinition()?.getName()).toEqual("Core API");
    });

    it('should correctly set API paths', async () => {
        const service = new ServiceServicePromiseClient(API_CORE_URL);
        const request = new GetServiceRequest().setId("core");

        const response = await service.get(request);

        let endpoints = response.getService()!.getApi()!.getEndpointsList()!;
        let validEndpoints = endpoints.filter((e: ServiceApiEndpoint , _, __): boolean => {
            return e.getType() == ApiType.WEBGRPC && e.getId() == "main"
        })
        expect(validEndpoints!.length).toEqual(1)

        let apiRootPath = validEndpoints![0].getPath()
        expect(`${API_HOST}${apiRootPath}`).toEqual(API_CORE_URL);
    });
});
