export const API_HOST = process.env['API_HOST'] || 'http://127.0.0.1:17440'
export const API_CORE_URL = `${API_HOST}/api/v1/core`;
